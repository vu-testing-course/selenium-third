package com.vutestingcourse.seleniumthird.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class HouseBookingPage {
  public static final String URL = "https://suninjuly.github.io/explicit_wait2.html";
  public static final String TITLE = "Simple registration form";
  public static final String MOTO = "Math is real magic!";
  public static final String SOLUTION_TEXT =
      "Congrats, you've passed the task! Copy this code as the answer to Stepik quiz";

  private final WebDriver driver;
  private final By priceSelector = By.id("price");
  private final By bookingButtonSelector = By.id("book");
  private final By motoSelector = By.id("simple_text");
  private final By variableSelector = By.id("input_value");
  private final By inputFieldSelector = By.id("answer");
  private final By submitSelector = By.id("solve");

  private WebElement moto;
  private WebElement submit;

  public HouseBookingPage(WebDriver driver) {
    this.driver = driver;
  }

  public void bookOnCorrectPrice() {
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));
    wait.until(ExpectedConditions.textToBe(priceSelector, "$100"));

    driver.findElement(bookingButtonSelector).click();
    moto = driver.findElement(motoSelector);
  }

  public String getMoto() {
    if (moto == null) {
      throw new IllegalStateException("Must book");
    }
    return moto.getText();
  }

  public void inputSolution() {
    if (moto == null) {
      throw new IllegalStateException("Must book");
    }

    WebElement inputField = driver.findElement(inputFieldSelector);
    submit = driver.findElement(submitSelector);

    inputField.sendKeys(getSolution());
    submit.click();
  }

  public String getCongratulationText() {
    if (moto == null) {
      throw new IllegalStateException("Must book");
    } else if (submit == null) {
      throw new IllegalStateException("Must submit");
    }

    Alert alert = driver.switchTo().alert();
    String result = alert.getText();
    alert.accept();
    return result;
  }

  // private methods

  private String getSolution() {
    WebElement variable = driver.findElement(variableSelector);
    double number = Double.parseDouble(variable.getText());
    double solution = Math.log(Math.abs(12 * Math.sin(number)));
    return String.valueOf(solution);
  }
}
