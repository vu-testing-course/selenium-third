package com.vutestingcourse.seleniumthird.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RegistrationPage {
  public static final String URL1 = "http://suninjuly.github.io/registration1.html";
  public static final String URL2 = "http://suninjuly.github.io/registration2.html";

  public static final String FIRST_NAME_PLACEHOLDER = "Input your first name";

  private final WebDriver driver;
  private final By firstNameFieldSelector = By.cssSelector(".first_block > div:nth-child(1) > input:nth-child(2)");

  public RegistrationPage(WebDriver driver) {
    this.driver = driver;
  }

  public String getFirstNamePlaceholder() {
    return driver.findElement(firstNameFieldSelector).getAttribute("placeholder");
  }
}
