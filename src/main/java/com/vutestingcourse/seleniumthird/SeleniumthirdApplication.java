package com.vutestingcourse.seleniumthird;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeleniumthirdApplication {

    public static void main(String[] args) {
        SpringApplication.run(SeleniumthirdApplication.class, args);
    }

}
