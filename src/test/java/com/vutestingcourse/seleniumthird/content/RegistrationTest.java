package com.vutestingcourse.seleniumthird.content;

import com.vutestingcourse.seleniumthird.pages.RegistrationPage;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class RegistrationTest {
  public static WebDriver driver;
  public static RegistrationPage registrationPage;

  @BeforeAll
  public static void setup() {
    driver = new ChromeDriver();
    driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
    driver.manage().window().maximize();

    registrationPage = new RegistrationPage(driver);
  }

  @AfterAll
  public static void tearDown() {
    driver.close();
    driver.quit();
  }

  @Test
  @Order(1)
  public void verifyFirstNamePlaceHolder_firstForm() {
    driver.get(RegistrationPage.URL1);

    String expected = RegistrationPage.FIRST_NAME_PLACEHOLDER;
    String actual = registrationPage.getFirstNamePlaceholder();
    Assertions.assertEquals(expected, actual);
  }

  @Test
  @Order(2)
  public void verifyFirstNamePlaceHolder_secondForm() {
    driver.get(RegistrationPage.URL2);

    String expected = RegistrationPage.FIRST_NAME_PLACEHOLDER;
    String actual = registrationPage.getFirstNamePlaceholder();
    Assertions.assertNotEquals(expected, actual); // Result is opposite to first form
  }
}
