package com.vutestingcourse.seleniumthird.flow;

import com.vutestingcourse.seleniumthird.pages.HouseBookingPage;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class HouseBookingFlowTest {
  public static WebDriver driver;
  public static HouseBookingPage bookingPage;

  @BeforeAll
  public static void setup() {
    driver = new ChromeDriver();
    driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
    driver.manage().window().maximize();

    bookingPage = new HouseBookingPage(driver);
    driver.get(HouseBookingPage.URL);
  }

  @AfterAll
  public static void tearDown() {
    driver.close();
    driver.quit();
  }

  @Test
  @Order(1)
  public void verifyPageTitle() {
    String expected = HouseBookingPage.TITLE;
    String actual = driver.getTitle();
    Assertions.assertEquals(expected, actual);
  }

  @Test
  @Order(2)
  public void verifyMoto() {
    bookingPage.bookOnCorrectPrice();

    String expected = HouseBookingPage.MOTO;
    String actual = bookingPage.getMoto();
    Assertions.assertEquals(expected, actual);
  }

  @Test
  @Order(3)
  public void verifySolution() {
    bookingPage.inputSolution();

    String expected = HouseBookingPage.SOLUTION_TEXT;
    String actual = bookingPage.getCongratulationText().split(": ")[0];
    Assertions.assertEquals(expected, actual);
  }
}
